package it.unibo.jvs;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import it.unibo.jvs.streamer.JVSStreamer;
import it.unibo.jvs.streamer.listeners.JVSStreamStatusListener;
import it.unibo.jvs.streamer.listeners.JVSResponseListener;
import it.unibo.jvs.streamer.streamer.R;
import it.unibo.jvs.streamer.utils.NetworkAddressUtils;

public class MainActivity extends AppCompatActivity implements JVSStreamStatusListener {

    //region Fields
    //constants
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int MY_REQUEST_CODE = 100;

    //interface fields
    private TextView statusText;
    private Button actionButton;

    //fields
    private JVSStreamer streamer;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        askPermissions();
        initInterface();

        actionButton.setOnClickListener(view -> {
            if (checkPermissions()) {

                //lazy initialization
                if (streamer == null) {
                    streamer = new JVSStreamer(MainActivity.this, MainActivity.this);
                }

                if (streamer.isRunning()) {
                    streamer.stop(new JVSResponseListener() {
                        @Override
                        public void handleSuccess() {
                            actionButton.setText(getString(R.string.start_button));
                            statusText.setText(getString(R.string.status_stopped));

                            showToast("Streaming stopped!");
                        }

                        @Override
                        public void handleFailure(Throwable throwable) {
                            showToast("Unable to stop streaming: " + throwable.getMessage());
                        }
                    });
                } else {
                    actionButton.setEnabled(false);
                    statusText.setText(getString(R.string.status_initializing));
                    streamer.start(new JVSResponseListener() {
                        @Override
                        public void handleSuccess() {
                            actionButton.setText(getString(R.string.stop_button));
                            statusText.setText(getString(R.string.status_running));
                            actionButton.setEnabled(true);

                            showToast("Streaming started!");
                        }

                        @Override
                        public void handleFailure(Throwable throwable) {
                            actionButton.setText(getString(R.string.start_button));
                            statusText.setText(getString(R.string.status_idle));
                            actionButton.setEnabled(true);

                            showToast("Unable to start streaming: " + throwable.getMessage());
                        }
                    });
                }
            } else {
                Log.d(TAG, "Cannot start the stream, not all needed permissions have been granted.");
                askPermissions();
            }
        });
        statusText.setText(getString(R.string.status_idle));

        final TextView addressText = findViewById(R.id.addressText);
        addressText.setText(NetworkAddressUtils.getLocalIpAddress(false));
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (streamer != null && streamer.isRunning()) {
            streamer.stop(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (streamer != null && streamer.isRunning()) {
            streamer.dispose();
        }
    }

    //region Permissions
    private void askPermissions() {

        List<String> permissionToRequest = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissionToRequest.add(Manifest.permission.CAMERA);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            permissionToRequest.add(Manifest.permission.RECORD_AUDIO);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionToRequest.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (permissionToRequest.size() > 0) {
            String[] arrayRequests = permissionToRequest.toArray(new String[0]);
            ActivityCompat.requestPermissions(this, arrayRequests, MY_REQUEST_CODE);
        }
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                return ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_REQUEST_CODE) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    showToast("Permission not granted for: " + permissions[i]);
                    finish();
                } else {
                    Log.d(TAG, "Permission granted for: " + permissions[i]);
                }
            }
        }
    }
    //endregion

    //region JVSStreamStatusListener
    @Override
    public void handleConnected() {
        showToast("Connection succeeded!");
    }

    @Override
    public void handleConnectionFailed(@NonNull String message) {

        actionButton.setText(getString(R.string.start_button));
        statusText.setText(getString(R.string.status_idle));

        showToast("Connection failed: " + message);
    }

    @Override
    public void handleDisconnected() {

        actionButton.setText(getString(R.string.start_button));
        statusText.setText(getString(R.string.status_idle));

        showToast("Disconnected from server!");
    }
    //endregion

    //region Helpers
    /**
     * Initializes the interface.
     */
    private void initInterface() {
         statusText = findViewById(R.id.statusText);
         actionButton = findViewById(R.id.actionButton);
    }

    /**
     * Shows a toast.
     * @param message The message to show.
     */
    private void showToast(final String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    //endregion
}