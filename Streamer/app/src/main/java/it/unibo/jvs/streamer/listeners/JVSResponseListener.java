package it.unibo.jvs.streamer.listeners;

/**
 * JVS Response listener.
 */
public interface JVSResponseListener {

    /**
     * Handles the success event.
     */
    void handleSuccess();

    /**
     * Handles the failure event.
     * @param throwable The cause.
     */
    void handleFailure(Throwable throwable);
}
