package it.unibo.jvs.streamer.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * NetworkAddressUtils class for ip addresses.
 */
public class NetworkAddressUtils {

    private static Pattern VALID_IPV4_PATTERN = Pattern.compile("(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])", Pattern.CASE_INSENSITIVE);
    private static Pattern VALID_IPV6_PATTERN = Pattern.compile("([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}", Pattern.CASE_INSENSITIVE);

    /**
     * Determines if the given string is a valid IPv4 or IPv6 address.  This method
     * uses pattern matching to see if the given string could be a valid IP address.
     *
     * @param ipAddress A string that is to be examined to verify whether or not
     *  it could be a valid IP address.
     * @return <code>true</code> if the string is a value that is a valid IP address,
     *  <code>false</code> otherwise.
     */
    public static boolean isIpAddress(String ipAddress) {
        Matcher m1 = NetworkAddressUtils.VALID_IPV4_PATTERN.matcher(ipAddress);
        if (m1.matches()) {
            return true;
        }
        Matcher m2 = NetworkAddressUtils.VALID_IPV6_PATTERN.matcher(ipAddress);
        return m2.matches();
    }

    /**
     * Determines whether the input ip address is IPV4 or not.
     * @param ipAddress The input ip address.
     * @return True, if the ip address is IPV4; otherwise false.
     */
    public static boolean isIpv4Address(String ipAddress) {
        Matcher m1 = NetworkAddressUtils.VALID_IPV4_PATTERN.matcher(ipAddress);
        return m1.matches();
    }

    /**
     * Determines whether the input ip address is IPV6 or not.
     * @param ipAddress The input ip address.
     * @return True, if the ip address is IPV6; otherwise false.
     */
    public static boolean isIpv6Address(String ipAddress) {
        Matcher m1 = NetworkAddressUtils.VALID_IPV6_PATTERN.matcher(ipAddress);
        return m1.matches();
    }

    /**
     * Returns the IP address of the first configured interface of the device
     * @param removeIPv6 If true, IPv6 addresses are ignored
     * @return the IP address of the first configured interface or null
     */
    public static String getLocalIpAddress(boolean removeIPv6) {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (inetAddress.isSiteLocalAddress() && !inetAddress.isAnyLocalAddress() &&
                            (!removeIPv6 || isIpv4Address(inetAddress.getHostAddress())) ) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ignore) {}
        return null;
    }
}
