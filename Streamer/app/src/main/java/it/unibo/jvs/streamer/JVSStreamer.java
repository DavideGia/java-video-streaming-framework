package it.unibo.jvs.streamer;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pedro.rtplibrary.rtsp.RtspCamera2;
import com.pedro.rtsp.utils.ConnectCheckerRtsp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.unibo.jvs.streamer.listeners.JVSResponseListener;
import it.unibo.jvs.streamer.listeners.JVSStreamStatusListener;
import it.unibo.jvs.streamer.utils.NetworkAddressUtils;

/**
 * Java Video Streaming Framework streamer.
 */
public class JVSStreamer implements ConnectCheckerRtsp {

    //region Enumerations
    /* backend service encoding type */
    private enum EncType {
        MPEG_DASH_PASSTHROUGH,
        MPEG_DASH,
        WEBM_DASH_VP8,
        WEBM_DASH_VP9
    }

    /* backend service rtsp mode */
    private enum RTSPMode {
        SERVER,
        CLIENT
    }
    //endregion

    //region Constants
    private static final String TAG = JVSStreamer.class.getSimpleName();

    /* general */
    private static final String SERVER_ADDRESS = "127.0.0.1";
    private static final String SERVER_PORT = "8081";
    private static final String SERVER_URL = String.format("http://%s:%s/streams", SERVER_ADDRESS, SERVER_PORT);
    private static final int DEFAULT_ENC_TYPE = EncType.MPEG_DASH.ordinal();
    private static final int DEFAULT_MODE = RTSPMode.SERVER.ordinal();

    /* video */
    private static final int DEFAULT_WIDTH = 1280;
    private static final int DEFAULT_HEIGHT = 720;
    private static final int DEFAULT_FPS = 25;
    private static final int DEFAULT_BITRATE = 2000 * 1024;

    /* audio */
    private static final int DEFAULT_ABITRATE = 64 * 1024;
    private static final int DEFAULT_SAMPLERATE = 16000;
    private static final boolean DEFAULT_STEREO = true;
    //endregion

    //region Fields
    /* extra arguments - use -180° rotation with Vuzix M300 => .put("-vf").put("transpose=2,transpose=2") */
    private final JSONArray DEFAULT_ADVOPT = new JSONArray();

    /* keep track of the streamID in case it's needed to send a
    kill request to the jvs service, only if the mode is set to SERVER */
    private int streamID = -1;
    /* flag to tell app to ignore connection errors if the app sent a stop request to the jvs server */
    private boolean ignoreConnectionErrors = false;
    /* save the current status of the stream */
    private boolean isStarted = false;

    private Handler mainHandler;
    private Context context;
    private RtspCamera2 rtspCamera2;
    private JVSStreamStatusListener streamStatusListener;
    //endregion

    /**
     * Default constructor
     * @param context The context.
     * @param streamStatusListener The listener for the current stream status.
     */
    public JVSStreamer(@NonNull Context context, @NonNull JVSStreamStatusListener streamStatusListener) {
        this.context = context;
        this.mainHandler = new Handler(context.getMainLooper());
        this.streamStatusListener = streamStatusListener;
    }

    //region Public methods
    /**
     * Starts a new stream.
     * @param listener The JVS response listener.
     */
    public void start(@NonNull final JVSResponseListener listener) {

        if (rtspCamera2 == null || !rtspCamera2.isStreaming()) {

            rtspCamera2 = new RtspCamera2(context, this);

            if (rtspCamera2.prepareAudio(DEFAULT_ABITRATE, DEFAULT_SAMPLERATE, DEFAULT_STEREO, true, true) &&
                    rtspCamera2.prepareVideo(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_FPS, DEFAULT_BITRATE, true, 0)) {
                sendRequestJVSServer(listener);
                //rtspCamera2.startStream("rtsp://" + SERVER_ADDRESS + ":5540/listen/live");
                isStarted = true;
            } else {
                final Throwable error = new Exception("This device do not support streaming");
                Log.e(TAG, "Unable to prepare stream", error);

                runOnMainThread(() -> listener.handleFailure(error));
            }
        } else {
            final Throwable error = new Exception("Another stream is still running");
            Log.e(TAG, "Unable to start stream", error);
            runOnMainThread(() -> listener.handleFailure(error));
        }
    }

    /**
     * Stops the current stream.
     * @param listener The JVS response listener.
     */
    public void stop(final JVSResponseListener listener) {
        if (isStarted) {
            try {
                if (DEFAULT_MODE == 0){
                    sendKillRequestJVSServer(listener);
                }
            } catch (Exception ex) {
                Log.e(TAG, "Unable to stop stream", ex);
                if (listener != null) {
                    runOnMainThread(() -> listener.handleFailure(ex));
                }
            }
        }
    }

    /**
     * Disposes this instance.
     */
    public void dispose() {
        stop(null);
        mainHandler = null;
        context = null;
    }

    /**
     * Defines whether a stream is active or not.
     * @return True, if a stream is currently active; otherwise false.
     */
    public boolean isRunning() {
        return this.isStarted;
    }
    //endregion

    //region Helpers
    /**
     * Sends a POST HTTP request to the JVS service
     */
    private synchronized void sendRequestJVSServer(@NonNull final JVSResponseListener listener) {
        Log.d(TAG, "Sending add request to JVS server...");

        String currDate = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.getDefault()).format(new Date());

        JSONObject body = null;
        try {
            JSONObject info = null;

            //stream info only if webm encoding selected
            if (DEFAULT_ENC_TYPE != 0 && DEFAULT_MODE == 0) {
                info = new JSONObject("{" +
                        "\"streams\": [" +
                "{" +
                    "\"index\": 0," +
                    "\"codec_name\": \"aac\"," +
                    "\"codec_type\": \"audio\"," +
                    "\"sample_rate\": \"" + DEFAULT_SAMPLERATE + "\"," +
                    "\"channels\": " + (DEFAULT_STEREO ? 2 : 1) + "," +
                    "\"bits_per_raw_sample\": 16" +
                "}," +
                "{" +
                    "\"index\": 1," +
                    "\"codec_name\": \"avc\"," +
                    "\"codec_type\": \"video\"," +
                    "\"width\": " + DEFAULT_WIDTH + "," +
                    "\"height\": " + DEFAULT_HEIGHT + "," +
                    "\"pix_fmt\": \"yuv420p\"," +
                    "\"r_frame_rate\": \"" + DEFAULT_FPS + "000\"" +
                "}" +
                "]" +
            "}");
            }

            body = new JSONObject()
                    .put("url", "rtsp://" + NetworkAddressUtils.getLocalIpAddress(true))
                    .put("encType", DEFAULT_ENC_TYPE)
                    .put("title", "Dummy stream #" + DEFAULT_ENC_TYPE + "-" + currDate)
                    .put("descr", "stream " + currDate)
                    .put("mode", DEFAULT_MODE)
                    .put("customArgs", DEFAULT_ADVOPT);

            if (DEFAULT_ENC_TYPE != 0 && DEFAULT_MODE == 0) {
                body.put("info", info);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Failed to prepare the request for the JVS server", e);
            runOnMainThread(() -> listener.handleFailure(e));
        }

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, SERVER_URL, body, response -> {
            try {
                if (response.getInt("status") == 0) {
                    Log.d(TAG, "Server setup completed, start announcing stream...");

                    String url = String.format(Locale.getDefault(), "rtsp://%s:%d/%s",
                            SERVER_ADDRESS, response.getInt("listPort"), response.getString("annPath"));

                    streamID = response.getInt("id");
                    rtspCamera2.startStream(url);
                    isStarted = true;

                    Log.d(TAG, "Stream has been successfully started");
                    runOnMainThread(listener::handleSuccess);
                } else {
                    Throwable error = new Exception(response.getString("message"));
                    runOnMainThread(() -> listener.handleFailure(error));
                    Log.e(TAG, "Unable to start streaming, server returned an error", error);
                }
            } catch (JSONException e) {
                Log.e(TAG, "Error detected while parsing server response",  e);
                runOnMainThread(() -> listener.handleFailure(e));
            }
        }, error -> {
            runOnMainThread(() -> listener.handleFailure(error));
            Log.e(TAG, "Stream request failed: ", error);
        });

        req.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }

    /**
     * Sends a PATCH HTTP request to the JVS service to stop rtsp server if the selected mode is SERVER
     */
    private synchronized void sendKillRequestJVSServer(final JVSResponseListener listener) {
        Log.d(TAG, "Sending stop request to JVS server...");

        if (streamID > 0 && isStarted) {
            ignoreConnectionErrors = true;

            String url = String.format(Locale.getDefault(), "%s/%d", SERVER_URL, streamID);
            RequestQueue queue = Volley.newRequestQueue(context);
            StringRequest req = new StringRequest(Request.Method.PATCH, url, response -> {
                Log.d(TAG, "The stream has been successfully stopped.");

                streamID = -1;
                ignoreConnectionErrors = isStarted = false;

                if (listener != null) {
                    runOnMainThread(listener::handleSuccess);
                }
            }, error -> {
                Log.e(TAG, "Unable to stop the stream", error);

                streamID = -1;
                ignoreConnectionErrors = isStarted = false;

                if (listener != null) {
                    runOnMainThread(() -> listener.handleFailure(error));
                }
            });

            req.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(req);
        } else {
            Log.d(TAG, "Skipped server kill request because no stream are currently active");
        }
    }

    /**
     * Executes the input runnable on the ui thread.
     * @param runnable The runnable to execute.
     */
    private void runOnMainThread(Runnable runnable) {
        mainHandler.post(runnable);
    }
    //endregion

    //region ConnectCheckerRtsp
    @Override
    public void onConnectionSuccessRtsp() {
        Log.d(TAG, "RTSP: connection succeeded");
        runOnMainThread(() -> streamStatusListener.handleConnected());
    }

    @Override
    public void onConnectionFailedRtsp(final String message) {
        Log.d(TAG, "RTSP: connection failed (" + message + ")");
        try {
            rtspCamera2.stopStream();
        } catch (Exception ex) {
            Log.e(TAG, "Unable to stop stream after a connection failure", ex);
        } finally {
            try {
                rtspCamera2.stopRecord();
            } catch (Exception e) {
                Log.e(TAG, "Unable to stop recording after a connection failure", e);
            } finally {
                isStarted = false;
                if (!ignoreConnectionErrors) {
                    runOnMainThread(() -> streamStatusListener.handleConnectionFailed(message));
                }
            }
        }
    }

    @Override
    public void onDisconnectRtsp() {
        Log.d(TAG, "RTSP: detected disconnection");
        try {
            rtspCamera2.stopRecord();
        } catch (Exception ex) {
            Log.e(TAG, "Unable to stop recording after a disconnection", ex);
        } finally {
            isStarted = false;
            runOnMainThread(() -> streamStatusListener.handleDisconnected());
        }
    }

    @Override
    public void onAuthErrorRtsp() {
        Log.d(TAG, "RTSP: authentication request failed");
    }

    @Override
    public void onAuthSuccessRtsp() {
        Log.d(TAG, "RTSP: authentication request succeeded");
    }
    //endregion
}
