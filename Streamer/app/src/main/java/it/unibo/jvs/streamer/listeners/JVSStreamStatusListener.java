package it.unibo.jvs.streamer.listeners;

import android.support.annotation.NonNull;

/**
 * JVS Stream status listener.
 */
public interface JVSStreamStatusListener {

    /**
     * Handles the connection succeeded event.
     */
    void handleConnected();

    /**
     * Handles the connection failed event.
     * @param message The message that describes the cause.
     */
    void handleConnectionFailed(@NonNull String message);

    /**
     * Handles the disconnection event.
     */
    void handleDisconnected();
}
